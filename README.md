# README #
This repository contains all the software components related with the Smart Light Management for Greenhouses project (SLM4G) funded by SAF2 - EU

## Actual version ##
* Version 0.0.1 (Alfa version)

## WIKI ##
You can find more information in the wiki page of the project

[Wiki of Smart Light Management for Greenhouses](https://bitbucket.org/ralcaide/slm4g/wiki/Home)


## Components: ##

SLM4Horticulturist: Application used by the Horticulturists and composed by a Frontend and a Backend.

* Backend: Developed on Ruby using the framework Sinatra. It should be deployed in a machine accessible by the Frontend. This backend communicates (REST Api) with an instance of ORION that contains the Context needed by the App and with FIspace. FIspace manage the communication the instance(s) of SLM4L and SLM4H

* Frontend: Developed on HTML+CSS+Javascript using the AngularJS framework.  The Frontend communicates with its backend via REST Api

SLM4LightExpert: Application used by a Light Expert to provide advice about the light received by the plants

* Backend: Developed using the same technology as SLM4Horticulturist and with the same requirements. This backend communicates only with FIspace and not with ORION. FIspace manage the communication with the instance(s) of SLM4Horticulturist

* Frontend: Developed using the same technology as SLM4H but with another features. See the user manual accessible via [Wiki of Smart Light Management for Greenhouses](https://bitbucket.org/ralcaide/slm4g/wiki/Home)

SLM4EnergyExpert: Application used by Energy Expert to provide advice about the energy consumed by the installation

* Backend: Similar to SLM4LightExpert

* Frontend: Similar to SLM4EnergyExpert



## Global Architecture ##

![Devices_FIWARE_Comm_Report_TRL6.jpeg](https://bitbucket.org/repo/G9zkBa/images/3624209105-Devices_FIWARE_Comm_Report_TRL6.jpeg)

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


## How to execute the components from local ##

SLM4Horticulturist Frontend (Widget): 

```
#!

install nodejs
go to the widget folder, for example ~/SLM4G/SLM4Horticulturist_App/widget-angularJS/widget
execute: nodejs ../scripts/web-server.js

After that, the application is accessible via localhost url -> http://localhost:8000/index.html#/
```

SLM4Horticulturist Backend

```
#!

install ruby
go to the backend folder, for example ~/SLM4G/SLM4Horticulturist_App/backend
execute: ruby backend.rb

After that, the backend is accessible via localhost:8081. The frontend can communicate with it using the provided REST API
```


The steps for execute SLM4LightExperts and SLM4EnergyExperts are the same as the steps for SLM4Horticulturist but these applications should listen in another ports. By default:

- SLM4LightExperts: Frontend: 9000, Backend: 9091
- SLM4EnergyExperts: Fronted: 7000, Backend: 7071