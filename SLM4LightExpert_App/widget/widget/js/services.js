'use strict';

/* Services */

var appServices = angular.module('SLM4L.services', []);

//Local address
//var SLM4LBackendURL = 'http://127.0.0.1:9091/:message';

//Public address (FIWARE: 130.206.119.125)

var SLM4LBackendURL = 'http://130.206.119.125:9091/:message';

appServices.value('version', '0.0.1');

appServices.factory('SLM4LBackendAPIService',
    function($rootScope, $http) {
        var SLM4LBackendAPI = {};

        SLM4LBackendAPI.adviceList = [];
        SLM4LBackendAPI.questionList = [];
        
        SLM4LBackendAPI.getAdviceHistory = function() {
            $http({
                method: 'GET',
                url: SLM4LBackendURL.replace(':message', 'adviceHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4LBackendAPI.adviceList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4LBackendAPI.adviceList.push(JSON.parse(value));                        
                    })    

                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving advices History');
            })
        }

        SLM4LBackendAPI.getQuestionHistory = function() {
            $http({
                method: 'GET',
                url: SLM4LBackendURL.replace(':message', 'questionHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4LBackendAPI.questionList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4LBackendAPI.questionList.push(JSON.parse(value));                        
                    })    

                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving questions History');
            })
        }
        


        SLM4LBackendAPI.postAdvice = function(advice) {
            SLM4LBackendAPI.adviceList.push(JSON.parse(advice));

            return $http({
                method: 'POST',
                url: SLM4LBackendURL.replace(':message', 'sendAdvice'),
                data: advice,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });
        }

        SLM4LBackendAPI.updateQuestionListStatus = function(questionList) {

            return $http({
                method: 'POST',
                url: SLM4LBackendURL.replace(':message', 'updateQuestionListStatus'),
                data: questionList,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });   
        }

        //SSE events communication with the backend. It is needed for receive the advices
        
        var processMessage = function(msg) {
            console.log("in process message");
            var jsonMessage = JSON.parse(msg.data);
            $rootScope.$broadcast('new-question', jsonMessage);
        }               

        var source = new EventSource(SLM4LBackendURL.replace(':message', 'requestedAdvice'));
        source.addEventListener('message', processMessage, false); //Type of the event should be 'message'. If not, the event is not catch. Another possibility is to use the source.onmessage       

    return SLM4LBackendAPI;
});
    



