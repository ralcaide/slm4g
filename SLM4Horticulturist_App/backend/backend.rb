#####################################################################################################################################################
# Created by ralcaide at 11.07.2015
# This backend it is developed using the Sinatra framework of Ruby and it will communicates as follow is explained:
# 1. It will receive requests from SLM4Horticulturists widget
# 2. It will receive requests from FISpace
# 3. It will generate requests to ORION (FIWARE) to get/post/create/update devices and commands
# 4. It will generate requests to FIspace asking for advices about energy consumption and light received by plants (horticulture)
#####################################################################################################################################################


########################################################### TODO ####################################################################################
# To encapsulate the Clients (FIWARE / FIspace / Other?) behavior in ruby classes
# To read the config data from a file or another sustainable method 
# To create a sustainable architecture of the API -> To implement all the needed resources / requests
# To improve the CORS access
#####################################################################################################################################################



# SLM4Horticulturist backend

require 'sinatra'
require 'rest-client'
#require 'json'
#gem 'nokogiri'
#require 'nokogiri'


# Configuration of the server
set :server, 'thin'
set :port, 8081
set :environment, :development

# Handling errors
set :show_exceptions, true
set :raise_errors, false

# This code here is not working
# response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve

orion_url = 'http://130.206.80.40:1026/ngsi10/queryContext'
#PayloadTest = '{ "entities": [{"type": "", "isPattern":"false", "id": "SLM4G_Temp1_ent"}], "attributes": []}'


# Get a particular Entity from Orion '{ "entities": [{"type": "", "isPattern":"false", "id": "SLM4G_Sinapse1_ent"}], "attributes": []}'
# Get a list of entities with a particular type '{ "entities": [{"type": "SinapseDevice", "isPattern":"true", "id": ".*"}], "attributes": []}'

payload_template = '{ "entities": [{"type": "typeTemplate", "isPattern": "isPatternTemplate", "id": "idTemplate"}], "attributes": []}'



headers = {"Fiware-Service" => "fiwareiot", :content_type => :json, :accept => :json, "X-Auth-Token" => "NULL"}

# Communication with SLM4H frontend and FIWARE

local = true;
# Communication with SLM4H with SLM4L and SLM4E without using FIspace
if local 
	# Local and testing adresses
	slm4l_url = 'http://127.0.0.1:9091/push' #TODO RAE: To extract this information for text file or from frontend
	slm4e_url = 'http://127.0.0.1:7071/push' #TODO RAE: To extract this information for text file or from frontend
else
# Public adresses (FIWARE: 130.206.119.125)
	slm4l_url = 'http://130.206.119.125:9091/push' #TODO RAE: To extract this information for text file or from frontend
	slm4e_url = 'http://130.206.119.125:7071/push' #TODO RAE: To extract this information for text file or from frontend
end

# To manage server side events to send advices to the frontend when it is needed
connections = []
adviceList = [] # Contains the history of advices
questionList = [] # Contains the history of questions


get '/adviceHistory',  provides: 'text/event-stream' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	
	return adviceList.to_json;

end
get '/questionHistory', provides: 'text/event-stream' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	
	return questionList.to_json;

end

get '/advice', provides: 'text/event-stream' do 
	response['Cache-Control'] = 'no-cache'
	response['Connection'] = 'keep-alive'
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	stream :keep_open do |out|

		connections << out
		#out.callback on stream close event
		out.callback {
			#delete the connection
			connections.delete(out)
		}
	end
end


post '/push' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read) # TODO RAE: To think about symbolize advantages -> JSON.parse(request.body.read, :symbolize_names => true)

	connections.each do |out| 	
		out << "data: #{message.to_json}\n\n" 
	end

	puts message
	message['message']['read'] = false;

	puts message

	adviceList << message.to_json 

	# It is necessary to send a status code 200 if everything was ok. If not, the other backend or FIspace server will receive an status 500, causing problems. 
	status 200
end


# End server side events management

get '/' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	return "Smart Light Management for Greenhouses backend is working correcty"

end

 
# The user invokes this resource when he wants to update his installation.
# Input : Get message with the name of the Entity(s) (Installation Name)
# Business: Post to SLM4G ORION. In this ORION are the SLM4G entities saved. ORION will return the required context with updated entitities
# Output: Return the ORION response to the SLM4H frontend. The frontend will extract the needed information and display it in the correct way to the Horticulturist 

get '/context' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve

	payload = replace_query_context_payload(payload_template, params)
    # Hard coded example -> To improve. Read the needed parameters from INI file and the data received in the request

    query_context_response =  RestClient.post orion_url, payload , headers
	#HardCodedTestResponse =  RestClient.post 'http://130.206.80.40:1026/ngsi10/queryContext', PayloadTest , "Fiware-Service" => "fiwareiot", "X-Auth-Token" => "NULL", :content_type => :json, :accept => :json
	return query_context_response

	# End hard coded example
end



# The following methods are using example data for testing and demonstrations purposes. The real backend should communicates with ORION, format the data
# and provide the data to the client in the same format as the example data  

# Method that provides example json data with Rooms, Smart Devices and Light Points. This method will be invoked only for testing and demonstration purposes
get '/contextExample' do
 	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
    jsonExample = File.read('example.json')
    #jsonExample = '{"contextResponses": [{"room": {"name": "Tomatoes", "dimX": "100", "dimY": "50", "description": "Tomatoes from spain"}}]}'

    return jsonExample 
 	#puts "In context example"

end

# Method that provides example json with historical data of Rooms, Smart Devices and Light Points. This method will be invoked only for testing and demonstation purposes
get '/contextHistoricalDataExample' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
    jsonExample = File.read('example_historical_data.json')
    #jsonExample = '{"contextResponses": [{"room": {"name": "Tomatoes", "dimX": "100", "dimY": "50", "description": "Tomatoes from spain"}}]}'

    return jsonExample
end

post '/sendQuestion' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read)
	
	#puts message['message']['light']

	if message['message']['light']
		puts "sending a light advice request"
		slm4l_response = RestClient.post slm4l_url, message.to_json 
	end

	if message['message']['energy']
		puts "sending an energy advice request"
		slm4e_response = RestClient.post slm4e_url, message.to_json
	end

	questionList << message.to_json

	status 200

	# If light = true -> To send a message to light expert backend -> FISPACE
	# if energy = true -> To send a message to energy expert backend -> FISPACE
end

post '/updateAdviceListStatus' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read)

	
	if message.length == adviceList.length
		adviceList = [];

		# It is necessary to push element by element. If not, the Advice List contains an extra level
		message.each do |i|
			adviceList << i.to_json
		end

	else
		puts "Client and Server Advice Lists have different length"
	end 	

	status 200
end





# Communication with FIWARE


# Communication with FIspace 

# Helper functions
def replace_query_context_payload(payload, parameters)

	#RAE TODO: To improve the following code -> foreach, DRY!
	
	payload = parameters.has_key?('type') ? (payload.gsub 'typeTemplate', parameters['type']) : (payload.gsub 'typeTemplate', '')

	payload = parameters.has_key?('isPattern') ? (payload.gsub 'isPatternTemplate', parameters['isPattern']) : (payload.gsub 'isPatternTemplate', '')

	payload = parameters.has_key?('id') ? (payload.gsub 'idTemplate', parameters['id']) : (payload.gsub 'idTemplate', '')

	return payload

end


# Error handling

error do
	"SLM4LightExpert error handle"
end


