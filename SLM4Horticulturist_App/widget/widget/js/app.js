'use strict';

// Declare app level module which depends on filters, and services
angular.module('SLM4H', [
  'ngRoute',
  'SLM4H.filters',
  'SLM4H.services',
  'SLM4H.directives',
  'SLM4H.controllers',
  'SLM4H.modal',
  'angularBootstrapNavTree',
  'chart.js',
  'angularAwesomeSlider',
  'pascalprecht.translate',
  'ui.bootstrap'
]).
config(['$routeProvider', function($routeProvider) {
	  $routeProvider.when('/', {templateUrl: 'partials/dashboard.html', controller: 'dashboardController'});
    $routeProvider.when('/rooms', {templateUrl: 'partials/rooms.html', controller: 'roomsController'});
    $routeProvider.when('/installation', {templateUrl: 'partials/installation.html', controller: 'installationController'});		
  	$routeProvider.when('/messages', {templateUrl: 'partials/messages.html', controller: 'messagesController'});
  	$routeProvider.when('/error', {templateUrl: 'partials/error.html', controller: 'errorController'});
  	$routeProvider.otherwise({redirectTo: '/error'});
}])
.config(function($translateProvider) {
    
    $translateProvider.translations('en', {
    "TITLE_DASHBOARD": "Smart Light Management for your greenhouse",
    "TITLE_DASHBOARD_GLOBAL_VIEW": "Global view of your greenhouse",
    "TITLE_DASHBOARD_PLAN": "2D plan of ",
    "BUTTON_EN": "English",
    "BUTTON_ES": "Spanish",
    "BUTTON_SW": "Swedish",
    "TITLE": "Smart Light Management for Horticulturists",
    "DASHBOARD_MENU": "Dashboard",
    "ROOMS_MENU": "Rooms",
    "MESSAGES_MENU": "Messages",
    "SUMMARY_DASHBOARD_1": "Welcome to the best system for controlling the lights of your greenhouse.  You are able to perform the following actions: ",
    "SUMMARY_DASHBOARD_2": "Dashboard: Global view of your greenhouse. 2D plan and the actual status of the lights and sensors",
    "SUMMARY_DASHBOARD_3": "Rooms: You can select a room and then, to see the historical sensors and light values, the energy consumed by the different systems and manage your lights (ON, OFF, Dimming or programs)",
    "SUMMARY_DASHBOARD_4": "Messages: You can ask for two kind of advices to grower and energy experts",
    "BUTTON_REFRESH": "Refresh",
    "HEADER_ROOMS": "Rooms page",
    "TITLE_LINE_CHARTS_ROOMS": "Sensors and Lights measurements of ",
    "WORD_ROOM": "room",
    "WORD_ROOMS": "rooms",
    "TITLE_DEVICES_PANEL": "Select the sensors and/or light devices to see their values and evolution across the time",
    "TITLE_SENSORS_TABLE": "Sensors linked to smart devices",
    "TITLE_LIGHTS_TABLE": "Light devices installed in the room",
    "HEADER_DEVICE_COLUMN": "Device name",
    "HEADER_SENSOR_COLUMN": "Sensor name",
    "HEADER_LIGHT_COLUMN": "Light device name",
    "HEADER_DUMB_LIGHT_ROW": "Light without smart management",
    "TITLE_PIE_CHARTS_ROOMS": "Energy consumed by ",
    "CHARTS_DIFFERENT_UNITS": "The displayed values have different units. This chart is only valid to compare the evolution of different sensors",
    "HEADER_TODAY": "Today",
    "HEADER_LAST_WEEK": "Last week",
    "HEADER_LAST_MONTH": "Last month",
    "TITLE_LIGHT_MANAGEMENT_ROOMS": "Light management of ",
    "SUMMARY_LIGHT_MANAGEMENT_ROOMS": "What do you wanna do with the smart lights of your garden?",
    "BUTTON_MANUAL": "Manual",
    "BUTTON_AUTOMATIC": "Automatic",
    "BUTTON_SAVE": "Save",
    "BUTTON_CANCEL": "Cancel",
    "INPUT_INTERVALS": "How many intervals",
    "INPUT_INTERVAL_START": "Beginning at",
    "INPUT_INTERVAL_END": "Finishing at",
    "BUTTON_ON": "ON",
    "BUTTON_OFF": "OFF",
    "BUTTON_DIMMING" : "Dimming",
    "HEADER_WRITE": "Ask for advice",
    "INPUT_LIGHT_ADVICE": "Light advice",
    "INPUT_ENERGY_ADVICE": "Energy advice",
    "INPUT_SUBJECT_QUESTION": "Subject of the question",
    "INPUT_QUESTION" : "Question:",
    "INPUT_QUESTION_EXAMPLE": "What is happening with my light?",
    "INPUT_DATA": "Data: ",
    "BUTTON_ASK": "Ask!",
    "CANVAS_NOT_SUPPORTED": "Sorry, your browser doesn't support canvas",
    "MESSAGES_WRITE": "Ask for advice",
    "MESSAGES_INBOX": "Inbox",
    "MESSAGES_SENT": "Sent",
    "HEADER_SUBJECT_COLUMN": "Subject",
    "HEADER_FROM_COLUMN": "From",
    "HEADER_TO_COLUMN": "To",
    "TO_LIGHT_EXPERT": "Light Expert",
    "TO_ENERGY_EXPERT": "Energy Expert", 
    "HEADER_DATE_COLUMN": "Date",
    "TITLE_INBOX_TABLE": "Inbox",
    "TITLE_MESSAGE_TABLE": "Message detail",
    "MESSAGES_INBOX_EMPTY" : "Your inbox is empty. You should ask for advice",
    "TITLE_SENT_TABLE": "Sent questions to experts",
    "MESSAGES_SENT_EMPTY": "You have not asked for any advice",
    "CROP_TYPE_TOMATOES": "The Tomatoes crop need: <br/><br/><p>Temperature = (25 - 35) ºC</p><p>Daylength = 10 hours</p> <p>PAR = (22 - 30) Moles/Day</p>",
    "CROP_TYPE_PEPPERS": "The Peppers crop need: <br/><br/><p>Temperature = (25 - 35) ºC</p><p>Daylength = 9 hours</p><p>PAR = (20 - 25) Moles/Day</p>",
    "CROP_TYPE_UNKNOWN": "We do not have information about this crop type",
    "MESSAGES_SENT_OK": "The question is correctly sent!",
    "MESSAGES_SENT_KO": "A problems occurs sending the question. Please ask to the system administrator",
    "MANAGEMENT_SAVED_OK" : "The modifications have been saved",
    "MANAGEMENT_SAVED_KO" : "You are not connected to the supplemental light system. Please ask to the system administrator"
    });

    
    $translateProvider.useStaticFilesLoader({
      prefix: 'languages/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
});

