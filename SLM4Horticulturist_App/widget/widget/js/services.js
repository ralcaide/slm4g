'use strict';

/* Services */

var appServices = angular.module('SLM4H.services', []);

//var SLM4HBackendURL = 'http://127.0.0.1:8081/:message';
var SLM4HBackendURL = 'http://130.206.119.125:8081/:message';

appServices.value('version', '0.0.1');

appServices.factory('SLM4HBackendAPIService',
    function($rootScope, $http) {
        var SLM4HBackendAPI = {};

        /*
        trackingAPI.createTracking = function(track) {
            return $http({
                method: 'POST',
                url: trackingBaseUrl.replace(':username', track.username),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param({description: track.description})
            });
        }
        */

        // Get example context data
        SLM4HBackendAPI.getContextExample = function() {
            return $http({
                method: 'GET',
                url: SLM4HBackendURL.replace(':message', 'contextExample')
            });
        }

        // Get example context historical data
         SLM4HBackendAPI.getContextHistoricalDataExample = function() {
            return $http({
                method: 'GET',
                url: SLM4HBackendURL.replace(':message', 'contextHistoricalDataExample')
            });
        }            

        SLM4HBackendAPI.getContextFromBackend = function(parameters) {
            return $http({
                method: 'GET',
                url: SLM4HBackendURL.replace(':message', 'context'),
                params: {isPattern: parameters['isPattern'], id: parameters['id'], type: parameters['type']}
            });
        }

        /*
        SLM4HBackendAPI.postToBackend = function(message, jsonObject) {
            return $http({
                method: 'POST',
                url: SLM4HBackendURL.replace(':message', message),
                headers: {'Content-Type': 'application/json'},
                data: $.param(jsonObject)
            });
        }*/

        /*
        SLM4HBackendAPI.getAdvice = function(message) {
            return $http({
                method: 'GET', 
                url: SLM4HBackendURL.replace(':message', message)
            });

        } */


        SLM4HBackendAPI.extractRooms = function(jsonObject) {
            //RAE TODO
            var roomList = [];
            //var roomsExample = [];
            angular.forEach(jsonObject.contextResponses, function(value, key) {
                roomList.push(SLM4HBackendAPI.extractRoom(value));
            });
            return roomList;
        }

        SLM4HBackendAPI.extractRoom = function (jsonRoom) {
            var room = {
                name: jsonRoom.room.name,
                dimX: jsonRoom.room.dimX,
                dimY: jsonRoom.room.dimY,
                cropType: jsonRoom.room.cropType,
                description: jsonRoom.room.description,
                smartDeviceList: [],
                lightDeviceList: [],
                electricalSystemList: [],
                timeStamps: jsonRoom.room.timeStamps
            };

            if (jsonRoom.room.smartDevices) {
                angular.forEach(jsonRoom.room.smartDevices, function(jsonSmartDevice, key) {
                    var smartDevice = SLM4HBackendAPI.extractSmartDevice(jsonSmartDevice);
                    room.smartDeviceList.push(smartDevice);
                });
            }

            if (jsonRoom.room.lightDevices) {
                angular.forEach(jsonRoom.room.lightDevices, function(jsonLightDevice, key) {
                    var lightDevice = SLM4HBackendAPI.extractLightDevice(jsonLightDevice);
                    room.lightDeviceList.push(lightDevice);
                });
            }

            if (jsonRoom.room.electricalSystems) {
                angular.forEach(jsonRoom.room.electricalSystems, function(jsonElectricalSystem, key) {
                    var electricalSystem = SLM4HBackendAPI.extractElectricalSystem(jsonElectricalSystem);
                    room.electricalSystemList.push(electricalSystem);
                });
            }

            return room;
        }

        SLM4HBackendAPI.extractSmartDevice = function(jsonSmartDevice) {
            var smartDevice = {
                name: jsonSmartDevice.smartDevice.name,
                posX: jsonSmartDevice.smartDevice.posX,
                posY: jsonSmartDevice.smartDevice.posY,
                sensorList: [],
                /*
                lightSensor: {value: jsonSmartDevice.smartDevice.lightSensor.value, unit: jsonSmartDevice.smartDevice.lightSensor.unit},
                temperatureSensor: {value: jsonSmartDevice.smartDevice.temperatureSensor.value, unit: jsonSmartDevice.smartDevice.temperatureSensor.unit},
                PARSensor: {value: jsonSmartDevice.smartDevice.PARSensor.value, unit: jsonSmartDevice.smartDevice.PARSensor.unit}, */
                lightDeviceList: [],
            };

            if (jsonSmartDevice.smartDevice.sensors) {
                angular.forEach(jsonSmartDevice.smartDevice.sensors, function(jsonSensor, key) {
                    var sensor = SLM4HBackendAPI.extractSensor(jsonSensor);
                    smartDevice.sensorList.push(sensor);
                });
            }                        

            if (jsonSmartDevice.smartDevice.lightDevices) {
                angular.forEach(jsonSmartDevice.smartDevice.lightDevices, function(jsonLightDevice, key) {
                    var lightDevice = SLM4HBackendAPI.extractLightDevice(jsonLightDevice);
                    smartDevice.lightDeviceList.push(lightDevice);
                });
            }

            return smartDevice;

        }

        SLM4HBackendAPI.extractLightDevice = function(jsonLightDevice) {
            var lightDevice = {
                name: jsonLightDevice.lightDevice.name,
                status: jsonLightDevice.lightDevice.status,
                value: jsonLightDevice.lightDevice.value,
                posX: jsonLightDevice.lightDevice.posX,
                posY: jsonLightDevice.lightDevice.posY,
                unit: jsonLightDevice.lightDevice.unit,
                displayChart: false, //Property that indicates if the chart should be displayed or not
                displayDimming: false, //Property that indicates if the dimming slider should be displayed
                automaticMode: false //False = Manual Mode, True = Automatic mode
            };

            return lightDevice;
        }

        SLM4HBackendAPI.extractElectricalSystem = function(jsonElectricalSystem) {
            var electricalSystem = {
                name: jsonElectricalSystem.electricalSystem.name,
                totalEnergy: jsonElectricalSystem.electricalSystem.totalEnergy,
                unit: jsonElectricalSystem.electricalSystem.unit
            };

            return electricalSystem;
        }

        SLM4HBackendAPI.extractSensor = function(jsonSensor) {
            var sensor = {
                type: jsonSensor.sensor.type,
                name: jsonSensor.sensor.name,
                value: jsonSensor.sensor.value,
                unit: jsonSensor.sensor.unit,
                displayChart: false //Property that indicates if the chart should be displayed or not
            };

            return sensor;
        }




        SLM4HBackendAPI.extractDeviceProperties = function(jsonObject) {
            var deviceProperties = []; 
            
            angular.forEach(jsonObject.contextResponses, function(value, key) {
                angular.forEach(value.contextElement.attributes, function(value, key) {
                    if(value.type != "command") {
                        deviceProperties.push([value.name, value.type, value.value]);
                    }
                });
            });
            return deviceProperties;
        }

        SLM4HBackendAPI.extractDevices = function(jsonObject) {
            var devices = [];

            angular.forEach(jsonObject.contextResponses, function (value, key) {
                var id = value.contextElement.id;
                var timeInstant = "Not provided";
                var position = "Not provided";   // Sinapse Devices should contain a GPS position
                var status = "Not provided";     // Sinapse Devices should contain a status -> EC is correctly working or not -> Green / red

                angular.forEach(value.contextElement.attributes, function(value, key) {
                    if (value.name == "TimeInstant") {
                        timeInstant = value.value; //RAE TODO: Filter to format the timeInstant
                    } else if (value.name == "Position") {
                        position = value.value; //RAE TODO: Filter to format the position
                    } else if (value.name == "Status") {
                        status = value.value; //RAE TODO: The status should be displayed as red or green -> color of the name or something like that
                    }
                });
                devices.push([id, timeInstant, position, status]);
            });
            return devices;
        }


        //SSE events communication with the backend. It is needed for receive the advices
        SLM4HBackendAPI.adviceList = [];
        SLM4HBackendAPI.questionList = [];
        
        SLM4HBackendAPI.getAdviceHistory = function() {
            $http({
                method: 'GET',
                url: SLM4HBackendURL.replace(':message', 'adviceHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4HBackendAPI.adviceList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4HBackendAPI.adviceList.push(JSON.parse(value));                        
                    })    
                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving advices History');
            })
        }

        SLM4HBackendAPI.getQuestionHistory = function() {
            $http({
                method: 'GET',
                url: SLM4HBackendURL.replace(':message', 'questionHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4HBackendAPI.questionList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4HBackendAPI.questionList.push(JSON.parse(value));                        
                    })    

                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving questions History');
            })
        }


        SLM4HBackendAPI.postQuestion = function(question) {
            SLM4HBackendAPI.questionList.push(JSON.parse(question)); //TODO RAE: The question should be added only when the POST is successful. 

            return $http({
                method: 'POST',
                url: SLM4HBackendURL.replace(':message', 'sendQuestion'),
                data: question,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });
        }

        SLM4HBackendAPI.updateAdviceListStatus = function(adviceList) {

            return $http({
                method: 'POST',
                url: SLM4HBackendURL.replace(':message', 'updateAdviceListStatus'),
                data: adviceList,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });   
        }

        var processMessage = function(msg) {
            console.log("in process message");
            //var jsonMessage = JSON.parse(msg.data);
            //$rootScope.$broadcast('new-advice', jsonMessage);

            SLM4HBackendAPI.adviceList.push(JSON.parse(msg.data));
            $rootScope.$broadcast('new-advice');
        }               

        var source = new EventSource(SLM4HBackendURL.replace(':message', 'advice'));
        source.addEventListener('message', processMessage, false); //Type of the event should be 'message'. If not, the event is not catch. Another possibility is to use the source.onmessage     

    return SLM4HBackendAPI;
});
    



