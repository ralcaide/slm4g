'use strict';

/* Controllers */

angular.module('SLM4H.controllers', ["chart.js"])

    .controller('navController',function ($scope, $rootScope, $window, $location, SLM4HBackendAPIService) {
        $scope.adviceList = null;
        $scope.firstTime = true;

        $scope.advicesToRead = 0;


        $rootScope.go = function(path) {
            $location.url(path);
        }

        $rootScope.$on('new-advice', function(event) {
            $scope.addAdvice();
        })

        $rootScope.$on('update-advices', function(event) {
            $scope.updateAdvicesToRead();
        })

        $scope.addAdvice = function() {
            $scope.adviceList = SLM4HBackendAPIService.adviceList;
            
            $scope.updateAdvicesToRead();
        } 

        $scope.updateAdvicesToRead = function() {
            
            $scope.advicesToRead = 0;

            if ($scope.adviceList != null) {
                angular.forEach($scope.adviceList, function(advice, key) {
                    if (advice.message.read != true) {
                        $scope.advicesToRead += 1;    
                    }
                })
                return true;
            } else {
                return false;
            } 
        }

        $scope.getMessagesHistory = function() {
            $scope.firstTime = false; 
            SLM4HBackendAPIService.getAdviceHistory();
            $scope.adviceList = SLM4HBackendAPIService.adviceList;
            $scope.updateAdvicesToRead();                                            
        }

    })
    .controller('languageController', function($scope, $translate) {

         $scope.changeLanguage = function(langkey) {
            $translate.use(langkey);
        }
    })
    .controller('installationController', function($scope, SLM4HBackendAPIService) {
        $scope.refreshInstallation = function() {
            //var parameters = {isPattern: 'false', id: $scope.deviceType};
            var parameters = {type: $scope.deviceType, id: '.*', isPattern: 'true'};

            $scope.deviceList = [];
            SLM4HBackendAPIService.getContextFromBackend(parameters)
                .success(function (data, status, headers, config) {
                    //$scope.deviceList = SLM4HBackendAPIService.extractDeviceProperties(data);
                    $scope.deviceList = SLM4HBackendAPIService.extractDevices(data);                                              
                })
                .error(function (data, status, headers, config) {
                    console.log('error in Refresh Installation -> ' + status);
                })

        } 

        $scope.displayDeviceProperties = function(deviceId) {
            var parameters = {id: deviceId, isPattern: 'false'};            

            $scope.deviceProperties = [];
            SLM4HBackendAPIService.getContextFromBackend(parameters)
                .success(function (data, status, headers, config) {
                    $scope.deviceProperties = SLM4HBackendAPIService.extractDeviceProperties(data);
                    //$scope.deviceList = SLM4HBackendAPIService.extractDevices(data);                                              
                })
                .error(function (data, status, headers, config) {
                    console.log('error in displayDeviceProperties -> ' + status);
            })


        }

        $scope.addDevice = function() {
        
        }

        $scope.removeDevice = function() {
        
        }

        $scope.open = function(deviceId) {
            $scope.displayDeviceProperties(deviceId);    
            $scope.showModal = true;
        }

        $scope.ok = function() {
            $scope.showModal = false;
        }

        $scope.cancel = function() {
            $scope.showModal = false;
        };

        /* Test : drawTestCanvas */
        $scope.drawTestCanvas = function() {
            var canvas = document.getElementById('testCanvas');
            var context = canvas.getContext('2d');

            //do cool things with canvas
            context.font = '40pt Calibri';
            context.fillStyle = 'blue';
            context.fillText('Hello World!', 150, 100);

            //draw a line
            context.beginPath();
            context.moveTo(100, 100);
            context.lineTo(200, 150);
            context.lineTo(300, 100);
            context.lineTo(300, 50);
            context.lineTo(100, 100);
            context.lineWidth = 2;
            context.strokeStyle = '#ff0000';
            context.lineCap = 'round';
            context.stroke();

            //draw an arc
            var x = canvas.width / 2; //x and y is the center of the arc
            var y = canvas.height / 2;
            var radius = 75;
            var startAngle = 0 * Math.PI;
            var endAngle = 1.9 * Math.PI;
            var counterClockwise = false;

            context.beginPath();
            context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
            context.lineWidth = 15;

            // line color
            context.strokeStyle = 'black';
            context.stroke();


            //To draw rectangle -> Important for greenhouses
            context.beginPath();
            context.rect(10, 10, 100, 100);
            context.fillStyle = 'yellow';
            context.fill();
            context.lineWidth = 7;
            context.strokeStyle = 'black';
            context.stroke();

            //circle
            var centerX = 45
            var centerY = 45
            var radius = 10

            context.beginPath();
            context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
            context.fillStyle = 'green';
            context.fill();
            context.lineWidth = 1;
            context.strokeStyle = '#003300';
            context.stroke();


            //
            //var canvas = document.getElementById('myCanvas');
            //var context = canvas.getContext('2d');

      // begin custom shape
            context.beginPath();
            context.moveTo(170, 80);
            context.bezierCurveTo(130, 100, 130, 150, 230, 150);
            context.bezierCurveTo(250, 180, 320, 180, 340, 150);
            context.bezierCurveTo(420, 150, 420, 120, 390, 100);
            context.bezierCurveTo(430, 40, 370, 30, 340, 50);
            context.bezierCurveTo(320, 5, 250, 20, 250, 50);
            context.bezierCurveTo(200, 5, 150, 20, 170, 80);

      // complete custom shape
            context.closePath();
            context.lineWidth = 5;
            context.strokeStyle = 'blue';
            context.stroke();

      // bind event handler to clear button
            document.getElementById('clear').addEventListener('click', function() {
                context.clearRect(0, 0, canvas.width, canvas.height);
            }, false);




        };
    })

    .controller('dashboardController', function($scope, $translate, SLM4HBackendAPIService) {

        $scope.treeData = [];
        $scope.roomList = [];
        $scope.firstTime = true;
        $scope.room = 0;


        $scope.refreshDashboard = function() {

            $scope.firstTime = false;
            SLM4HBackendAPIService.getContextExample()
                .success(function (data, status, headers, config) {
                    $scope.roomList = SLM4HBackendAPIService.extractRooms(data);
                    $scope.drawGarden2D();
                    $scope.drawTree();                                              
                })
                .error(function (data, status, headers, config) {
                    console.log('error in Refresh Installation -> ' + status);
                })

        };

        $scope.drawGarden2D = function() {
            if ($scope.roomList.length <= 0) {
                return;
            }

            //Init size variables:
             var smartDeviceSize = 15;
             var lightDeviceSize = 8;
            
            //Init canvas
            var canvas = document.getElementById('garden2D');
            var context = canvas.getContext('2d');


            //To draw room
            var roomToDraw = $scope.roomList[$scope.room];
            
            var proportionalFactor = roomToDraw.dimY / roomToDraw.dimX;

            canvas.height = canvas.width * proportionalFactor;

            var xFactor = canvas.width / roomToDraw.dimX;
            var yFactor = canvas.height / roomToDraw.dimY;

            context = canvas.getContext('2d');


            context.beginPath();
            context.rect(0, 0, roomToDraw.dimX * xFactor, roomToDraw.dimY * yFactor);
            context.fillStyle = '#B5BDBE';
            context.fill();
            context.stroke();

            //To draw SmartDevice
            
            angular.forEach(roomToDraw.smartDeviceList, function(smartDeviceToDraw, key) {
                context.beginPath();
                var a = (smartDeviceToDraw.posX * xFactor) - (smartDeviceSize / 2);
                var b = (smartDeviceToDraw.posY * yFactor) - (smartDeviceSize / 2);
                var c = smartDeviceSize;
                var d = smartDeviceSize;

                context.rect(a, b, c ,d);
                context.lineWidth = 2;
                context.strokeStyle = '#FF6900'
                context.fillStyle = '#78BE20';
                context.fill();
                context.stroke();

                angular.forEach(smartDeviceToDraw.lightDeviceList, function(lightDeviceToDraw, key) {
                    var x = lightDeviceToDraw.posX * xFactor;
                    var y = lightDeviceToDraw.posY * yFactor;
                    context.beginPath();
                    context.arc(x, y, lightDeviceSize, 0, 2 * Math.PI, false);
                    context.lineWidth = 2;
                    context.strokeStyle = '#78BE20'
                    context.fillStyle = 'yellow';
                    context.fill();
                    context.stroke();
                });
            });


            // TODO RAE: Refactor   
            angular.forEach(roomToDraw.lightDeviceList, function(lightDeviceToDraw, key) {
                    var x = lightDeviceToDraw.posX * xFactor;
                    var y = lightDeviceToDraw.posY * yFactor;
                    context.beginPath();
                    context.arc(x, y, lightDeviceSize, 0, 2 * Math.PI, false);
                    context.lineWidth = 2;
                    context.strokeStyle = 'black'
                    context.fillStyle = 'yellow';
                    context.fill();
                    context.stroke();
            });
        }; 

        $scope.drawGardenLegend = function() {

        }

        $scope.drawTree = function() {
            $scope.treeData = [];

            if ($scope.roomList.length <= 0) {
                return;
            }
           
            //TODO RAE: To improve this algorithm    
            angular.forEach($scope.roomList, function(room, key) {
                var roomItem = {
                    label: room.name,
                    children : [],
                    data: {
                        description: room.description,
                        cropType: room.cropType,
                        id: key
                    }

                };

                var roomId = key;

                angular.forEach(room.smartDeviceList, function(smartDevice,key) {
                    
                    var smartDeviceItem = {
                        label: smartDevice.name,
                        children : [],
                        data: {
                            description: smartDevice.description,
                            id: roomId
                        },
                        //classes: ["orange"]
                    };

                    angular.forEach(smartDevice.sensorList, function(sensor, key) {
                        var sensorItem = {
                            label : sensor.name,
                            data: {
                                description: sensor.value + " (" + sensor.unit + ")",
                                id: roomId
                            },
                            //classes: ["blue"]
                        };

                        smartDeviceItem.children.push(sensorItem);
                    })


                    angular.forEach(smartDevice.lightDeviceList, function(lightDevice, key) {
                        var lightDeviceItem = {
                            label : lightDevice.name,
                            data: {
                                description: lightDevice.value + " (" + lightDevice.unit + ")",
                                id: roomId
                            },
                            //classes: ["yellow"] 
                        };

                        smartDeviceItem.children.push(lightDeviceItem);
                    })

                   roomItem.children.push(smartDeviceItem); 

                })

                 $scope.treeData.push(roomItem);

            });


        };

        $scope.treeHandler = function(branch) {
            var _ref;
            $scope.output = branch.label;
            $scope.cropType = 0;
            
            if (branch.data.id != null && $scope.room != branch.data.id) {
                $scope.room = branch.data.id;
                $scope.drawGarden2D();
            }

            if ((_ref = branch.data) != null ? _ref.cropType : void 0) {
                $scope.cropType = branch.data.cropType;
            }            

            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += ": " + branch.data.description + " ";
            }



    }; 

    })


    .controller('roomsController', function($scope, SLM4HBackendAPIService) {
        $scope.roomList = [];
        $scope.smartDevicesCheckList = [];
        $scope.room = 0;
        $scope.firstTime = true;

        $scope.saved = false;

        //Test number of automatic intervals
        $scope.intervals = 0;;

        $scope.getIntervals = function(intervals) {
            return new Array(intervals);
        }

        $scope.selectRoom = function(index) {
            $scope.room = index;

        }

        //Test data for awesome slider
        $scope.sliderValue = "10";
        $scope.sliderOptions = {
            from: 0, 
            to: 100, 
            step: 10,
            floor: true,
            skin: 'blue', 
            dimension: "%", 
            scale: [0, '|', 50, '|' , 100],
            css: {
                background: {"background-color": "silver"},
                before: {"background-color": "yellow"},
                default: {"background-color": "yellow"},
                after: {"background-color": "yellow"},
                pointer: {"background-color": "green"}
            }  
        }; 



        $scope.refreshRooms = function() {

            $scope.firstTime = false;
            SLM4HBackendAPIService.getContextHistoricalDataExample()
                .success(function (data, status, headers, config) {
                    $scope.roomList = SLM4HBackendAPIService.extractRooms(data);
                    $scope.refreshSensorCharts();
                    $scope.refreshLightDeviceCharts();
                    $scope.refreshEnergyCharts();
                })
                .error(function (data, status, headers, config) {
                    console.log('error in Refresh Installation -> ' + status);
                })

        };

        $scope.refreshSensorCharts = function() {
            $scope.sensorLabels = [];
            $scope.sensorSeries = [];
            $scope.sensorData = [];

            $scope.sensorChartDifferentUnits = false;
            var previousUnit = null;

            $scope.sensorLabels = $scope.roomList[$scope.room].timeStamps;

            angular.forEach($scope.roomList[$scope.room].smartDeviceList, function(smartDevice, key) {
                  angular.forEach(smartDevice.sensorList, function(sensor, key) {
                        if(sensor.displayChart) {
                            $scope.sensorSeries.push(sensor.name + " (" + sensor.unit + ")");
                            $scope.sensorData.push(sensor.value);

                            if(sensor.unit != previousUnit && previousUnit != null) {
                                $scope.sensorChartDifferentUnits = true;
                            }
                            previousUnit = sensor.unit;
                        }
                  });  
            });

        }

        $scope.activateDimming = function(lightDevice, value) {
            lightDevice.displayDimming = value;
        }

        $scope.activateAutomaticMode = function(lightDevice, value) {
            lightDevice.automaticMode = value;
        }

        $scope.refreshLightDeviceCharts = function() {
            $scope.lightDeviceLabels = [];
            $scope.lightDeviceSeries = [];
            $scope.lightDeviceData = [];

            $scope.lightDeviceLabels = $scope.roomList[$scope.room].timeStamps;

            angular.forEach($scope.roomList[$scope.room].smartDeviceList, function(smartDevice, key) {
                  angular.forEach(smartDevice.lightDeviceList, function(lightDevice, key) {
                        if(lightDevice.displayChart) {
                            $scope.lightDeviceSeries.push(lightDevice.name + " (" + lightDevice.unit + ")");
                            $scope.lightDeviceData.push(lightDevice.value);
                        }
                  });  
            });

            angular.forEach($scope.roomList[$scope.room].lightDeviceList, function(lightDevice, key) {
                        if(lightDevice.displayChart) {
                            $scope.lightDeviceSeries.push(lightDevice.name + " (" + lightDevice.unit + ")");
                            $scope.lightDeviceData.push(lightDevice.value);
                        }
            });

        }

        $scope.refreshEnergyCharts = function() {
            // This method fill the necessary data for display a Pie chart. Series data is not necessary
            $scope.electricalSystemLabels = [];
            $scope.electricalSystemData = [];

            angular.forEach($scope.roomList[$scope.room].electricalSystemList, function(electricalSystem, key) {
                        $scope.electricalSystemLabels.push(electricalSystem.name + " (" + electricalSystem.unit + ")");
                        $scope.electricalSystemData.push(electricalSystem.totalEnergy);
            });

        }

        $scope.save = function() {
            $scope.saved = true;
            $scope.result = false; //TODO -> When the software is connected to HW the result can be true. To Improve.

        }

        $scope.close = function() {
            $scope.saved = false;
        }

    })
    
    .controller('messagesController', function($rootScope, $scope, SLM4HBackendAPIService) {
        $scope.roomList = [];
        $scope.firstTime = true;
        $scope.adviceSelected = 0;
        $scope.adviceList = [];
        $scope.questionSelected = 0;
        $scope.questionList = [];


        $scope.displayWrite = true;
        $scope.displayInbox = false;
        $scope.displaySent = false;

        $scope.firstTime = true;
        $scope.sent = false;

        $scope.setDisplay = function(display) {
            $scope.eraseDisplay();

            if (display == 'write') {
                $scope.displayWrite = true;
            } else if (display == 'inbox') {
                $scope.displayInbox = true;
            } else if (display == 'sent') {
                $scope.displaySent = true;                
            }
        } 

        $scope.eraseDisplay = function() {
            $scope.displayWrite = false;
            $scope.displayInbox = false;
            $scope.displaySent = false;
        }        
        /*
        $scope.advice = SLM4HBackendAPIService.getAdvice;
        $scope.clickS = function () {
            $scope.advice2 = SLM4HBackendAPIService.update();
        }; */

        $rootScope.$on('new-advice', function(event) {
            $scope.addAdvice();
        });

        $scope.selectAdvice = function(index) {
            $scope.adviceSelected = index;

            $scope.adviceList[$scope.adviceSelected].message.read = !$scope.adviceList[$scope.adviceSelected].message.read; //The message change to unread <-> read for each click
            $rootScope.$broadcast('update-advices');

            SLM4HBackendAPIService.updateAdviceListStatus($scope.adviceList)
              .success(function (data, status, headers, config) {
                    console.log("Advice List updated correctly");
                })
                .error(function (data, status, headers, config) {
                    console.log("Error updating Advice List");
                }) 
        }

        $scope.addAdvice = function() {
            //$scope.adviceList.push(data);
            $scope.adviceList = SLM4HBackendAPIService.adviceList;
            $scope.$apply();

        }


        $scope.refreshRooms = function() {

            $scope.firstTime = false;
            SLM4HBackendAPIService.getContextHistoricalDataExample()
                .success(function (data, status, headers, config) {
                    $scope.roomList = SLM4HBackendAPIService.extractRooms(data);
                })
                .error(function (data, status, headers, config) {
                    console.log('error in Refresh Installation -> ' + status);
                })

        }; 

        $scope.sendQuestion = function(data) {
            
            //Data form form and add date. The format should be {message: {}}
            var question = {message: {}};
            question.message = data;
            question.message.date = new Date();
            question.message.from = "Ale farmer"; //TODO RAE: This value should be set by the farmer in the configuration page

            var jsonQuestion = angular.toJson(question);

            SLM4HBackendAPIService.postQuestion(jsonQuestion)
                .success(function(data, status, headers, config) {
                    console.log("Question sent correctly");
                    $scope.question = null;
                    $scope.sent = true;
                    $scope.result = true;

                })
                .error(function (data, status, headers, config) {
                    console.log('Error sending the question');
                    $scope.sent = true;
                    $scope.result = false;
                })
        }

         $scope.selectQuestion = function(index) {
            $scope.questionSelected = index;
        }

        $scope.getMessagesHistory = function() {
            $scope.firstTime = false; 
            
            SLM4HBackendAPIService.getAdviceHistory();
            SLM4HBackendAPIService.getQuestionHistory();

            $scope.adviceList = SLM4HBackendAPIService.adviceList;
            $scope.questionList = SLM4HBackendAPIService.questionList
        }

        $scope.close = function() {
            $scope.sent = false;
        }


    })

    .controller('errorController', function($scope, SLM4HBackendAPIService) {


    });