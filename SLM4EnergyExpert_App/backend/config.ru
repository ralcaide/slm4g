ENV['RACK_ENV'] = "production"

require File.expand_path '../backend.rb', __FILE__

run Sinatra::Application
