README of the backend of SLM4LightExpert Application

The backend of this application implementes a REST API that can be invoked using FIspace.

The REST API is offered using the Sinatra framework developed in Ruby. The following gems should be installed in order to execute the application:

- Sinatra + (Rack -> Installed automatically via Sinatra)
- Thin (server)
- Nokogiri 