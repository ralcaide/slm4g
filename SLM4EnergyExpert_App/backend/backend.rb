# SLM4Horticulturist backend

require 'sinatra'
require 'rest-client'
#gem 'nokogiri'
#require 'nokogiri'


# Configuration of the server
set :server, 'thin'
set :port, 7071
set :environment, :development

# Handling errors
set :show_exceptions, true
set :raise_errors, false

# TODO configuration with FISPACE

# Provisional ->  Hard coded client
# Local addresses
local = true

if local
	slm4h_url = 'http://127.0.0.1:8081/push'
else
# Public addresses (FIWARE: 130.206.119.125)
	slm4h_url = 'http://130.206.119.125:8081/push'
end

# To manage server side events to send advices to the frontend when it is needed
connections = []
adviceList = [] # Contains the history of advices
questionList = [] # Contains the history of questions

get '/adviceHistory',  provides: 'text/event-stream' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	
	return adviceList.to_json;

end

get '/questionHistory', provides: 'text/event-stream' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	
	return questionList.to_json;

end


get '/requestedAdvice', provides: 'text/event-stream' do 
	response['Cache-Control'] = 'no-cache'
	response['Connection'] = 'keep-alive'
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	stream :keep_open do |out|
		connections << out
		#out.callback on stream close event
		out.callback {
			#delete the connection
			connections.delete(out)
		}
	end
end


post '/push' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read)

	connections.each do |out| 
		out << "data: #{message.to_json}\n\n" 
	end

	questionList << message.to_json 

	status 200 #It is necessary to avoid problems in backends accessing this resource
end

post '/sendAdvice' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read)
	
	slm4h_response =  RestClient.post slm4h_url, message.to_json 

	#TODO RAE: Send it through FISPACE

	adviceList << message.to_json

	status 200

end 


post '/updateQuestionListStatus' do
	response['Access-Control-Allow-Origin'] = '*' # It allows CORS access -> To improve
	message = JSON.parse(request.body.read)

	
	if message.length == questionList.length
		questionList = [];

		# It is necessary to push element by element. If not, the Advice List contains an extra level
		message.each do |i|
			questionList << i.to_json
		end

	else
		puts "Client and Server Question Lists have different length"
	end 	

	status 200
end


# Error handling

error do
	"SLM4EnergyExpert error handle"
end