'use strict';

// Declare app level module which depends on filters, and services
angular.module('SLM4E', [
  'ngRoute',
  'SLM4E.filters',
  'SLM4E.services',
  'SLM4E.directives',
  'SLM4E.controllers',
  'pascalprecht.translate',
  'ui.bootstrap'
]).
config(['$routeProvider', function($routeProvider) {
	  $routeProvider.when('/', {templateUrl: 'partials/messages.html', controller: 'messagesController'});
}])
.config(function($translateProvider) {

    $translateProvider.translations('en', {
    "BUTTON_EN": "English",
    "BUTTON_ES": "Spanish",
    "BUTTON_SW": "Swedish",
    "TITLE": "Smart Light Management for Energy Experts",
    "MESSAGES_MENU": "Messages",
    "MESSAGES_WRITE": "Provide an advice",
    "MESSAGES_INBOX": "Inbox",
    "MESSAGES_SENT": "Sent",
    "HEADER_WRITE": "Provide an advice",
    "INPUT_SUBJECT_ADVICE": "Subject: ",
    "INPUT_TEXT_ADVICE": "Advice: ",
    "BUTTON_SEND": "Send!",
    "HEADER_INBOX": "Questions received",
    "TITLE_INBOX_TABLE": "List of questions",
    "TITLE_INBOX_MESSAGE_TABLE": "Detail of the question",
    "HEADER_SUBJECT_COLUMN": "Subject",
    "HEADER_FROM_COLUMN": "From",
    "HEADER_DATE_COLUMN": "Date",
    "HEADER_TO_COLUMN": "To",
    "HEADER_SENT": "Advices provided",
    "TITLE_SENT_TABLE": "List of advices",
    "TITLE_SENT_MESSAGE_TABLE": "Detail of the advice",
    "BUTTON_REPLY": "Reply",
    "MESSAGES_INBOX_EMPTY": "You do not have received any message",
    "MESSAGES_SENT_EMPTY": "You do not have sent any message",
    "MESSAGES_SENT_OK": "The advice is correctly sent!",
    "MESSAGES_SENT_KO": "A problems occurs sending the advice. Please ask to the system administrator"
    });
    
    $translateProvider.useStaticFilesLoader({
      prefix: 'languages/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');
});

