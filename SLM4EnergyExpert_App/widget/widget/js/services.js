'use strict';

/* Services */

var appServices = angular.module('SLM4E.services', []);

//Local address
var SLM4EBackendURL = 'http://127.0.0.1:7071/:message';

//Public address (FIWARE: 130.206.119.125)

//var SLM4EBackendURL = 'http://130.206.119.125:9091/:message';

appServices.value('version', '0.0.1');

appServices.factory('SLM4EBackendAPIService',
    function($rootScope, $http) {
        var SLM4EBackendAPI = {};

        SLM4EBackendAPI.adviceList = [];
        SLM4EBackendAPI.questionList = [];
        
        SLM4EBackendAPI.getAdviceHistory = function() {
            $http({
                method: 'GET',
                url: SLM4EBackendURL.replace(':message', 'adviceHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4EBackendAPI.adviceList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4EBackendAPI.adviceList.push(JSON.parse(value));                        
                    })    

                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving advices History');
            })
        }

        SLM4EBackendAPI.getQuestionHistory = function() {
            $http({
                method: 'GET',
                url: SLM4EBackendURL.replace(':message', 'questionHistory')
            }).success(function(data, status, headers, config) {
                if (!SLM4EBackendAPI.questionList.length) {
                    angular.forEach(data, function(value,key) {
                        SLM4EBackendAPI.questionList.push(JSON.parse(value));                        
                    })    

                }   
            }).error(function (data, status, headers, config) {
                console.log('Error retrieving questions History');
            })
        }
        


        SLM4EBackendAPI.postAdvice = function(advice) {
            SLM4EBackendAPI.adviceList.push(JSON.parse(advice));

            return $http({
                method: 'POST',
                url: SLM4EBackendURL.replace(':message', 'sendAdvice'),
                data: advice,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });
        }

        SLM4EBackendAPI.updateQuestionListStatus = function(questionList) {

            return $http({
                method: 'POST',
                url: SLM4EBackendURL.replace(':message', 'updateQuestionListStatus'),
                data: questionList,
                headers: {'Content-Type': 'text/plain'} //RAE TODO: This is necessary because with the 'application/json' header the POST is converted to OPTIONS. It is necessary to research about that
            });   
        }

        //SSE events communication with the backend. It is needed for receive the advices
        
        var processMessage = function(msg) {
            console.log("in process message");
            var jsonMessage = JSON.parse(msg.data);
            $rootScope.$broadcast('new-question', jsonMessage);
        }               

        var source = new EventSource(SLM4EBackendURL.replace(':message', 'requestedAdvice'));
        source.addEventListener('message', processMessage, false); //Type of the event should be 'message'. If not, the event is not catch. Another possibility is to use the source.onmessage       

    return SLM4EBackendAPI;
});
    



