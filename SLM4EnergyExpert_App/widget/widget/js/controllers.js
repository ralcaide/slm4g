'use strict';

/* Controllers */

angular.module('SLM4E.controllers', [])
     .controller('navController',function ($scope, $rootScope, $window, $location, SLM4EBackendAPIService) {
        $scope.QuestionList = [];
        $scope.firstTime = true;

        $scope.questionsToRead = 0;

        $rootScope.go = function(path){
            $location.url(path);
        }

         $rootScope.$on('new-question', function(event) {
            $scope.addQuestion();
        })

        $rootScope.$on('update-questions', function(event) {
            $scope.updateQuestionsToRead();
        })

        $scope.addQuestion = function() {
            $scope.questionList = SLM4EBackendAPIService.questionList;
            
            $scope.updateQuestionsToRead();
        } 

        $scope.updateQuestionsToRead = function() {
            
            $scope.questionsToRead = 0;

            if ($scope.questionList != null) {
                angular.forEach($scope.questionList, function(question, key) {
                    if (question.message.read != true) {
                        $scope.questionsToRead += 1;    
                    }
                })
                return true;
            } else {
                return false;
            } 
        }

        $scope.getMessagesHistory = function() {
            $scope.firstTime = false; 
            SLM4EBackendAPIService.getQuestionHistory();
            $scope.questionList = SLM4EBackendAPIService.questionList;
            $scope.updateQuestionsToRead();                                            
        }
    })

    .controller('languageController', function($scope, $translate) {

         $scope.changeLanguage = function(langkey) {
            $translate.use(langkey);
        }
    })  
    .controller('messagesController', function($rootScope, $scope, SLM4EBackendAPIService) {
        $scope.firstTime = true;
        $scope.adviceSelected = 0;
        $scope.adviceList = [];
        $scope.questionSelected = 0;
        $scope.questionList = [];


        $scope.displayWrite = true;
        $scope.displayInbox = false;
        $scope.displaySent = false;

        $scope.sent = false;

        $scope.firstTime = true;

        $scope.setDisplay = function(display) {
            $scope.eraseDisplay();

            if (display == 'write') {
                $scope.displayWrite = true;
            } else if (display == 'inbox') {
                $scope.displayInbox = true;
            } else if (display == 'sent') {
                $scope.displaySent = true;                
            }
        } 

        $scope.eraseDisplay = function() {
            $scope.displayWrite = false;
            $scope.displayInbox = false;
            $scope.displaySent = false;
        }        

        
        /*
        $scope.advice = SLM4HBackendAPIService.getAdvice;
        $scope.clickS = function () {
            $scope.advice2 = SLM4HBackendAPIService.update();
        }; */

        $rootScope.$on('new-question', function(event, data) {
            $scope.addQuestion(data);
        });

         $scope.selectQuestion = function(index) {
            $scope.questionSelected = index;

            $scope.questionList[$scope.questionSelected].message.read = !$scope.questionList[$scope.questionSelected].message.read; //The message change to unread <-> read for each click
            $rootScope.$broadcast('update-advices');

            SLM4EBackendAPIService.updateQuestionListStatus($scope.questionList)
              .success(function (data, status, headers, config) {
                    console.log("Question List updated correctly");
                })
                .error(function (data, status, headers, config) {
                    console.log("Error updating question List");
                }) 
        }

        $scope.addQuestion = function(data) {
            $scope.questionList.push(data);
            $scope.$apply();
        } 

        $scope.sendAdvice = function(data) {
            
            //Data from form and add date. The format should be {message: {}}
            var advice = {message: {}};
            advice.message = data;
            advice.message.date = new Date();
            advice.message.light = true;
            advice.message.from = "Purdue University"; //TODO RAE: The sender should be dynamic and configured by the light expert 

            var jsonAdvice = angular.toJson(advice);

            SLM4EBackendAPIService.postAdvice(jsonAdvice)
                .success(function(data, status, headers, config) {
                    console.log("Advice sent correctly");
                    $scope.advice = null;
                    $scope.sent = true;
                    $scope.result = true;
                })
                .error(function (data, status, headers, config) {
                    console.log('Error sending the Advice');
                    $scope.sent = true;
                    $scope.result = false;
                })
        }

        $scope.selectAdvice = function(index) {
            $scope.adviceSelected = index;
        }

        $scope.getMessagesHistory = function() {
            $scope.firstTime = false; 
            
            SLM4EBackendAPIService.getAdviceHistory();
            SLM4EBackendAPIService.getQuestionHistory();

            $scope.adviceList = SLM4EBackendAPIService.adviceList;
            $scope.questionList = SLM4EBackendAPIService.questionList
        }

        $scope.close = function() {
            $scope.sent = false;
        }


    })
    .controller('errorController', function($scope, SLM4EBackendAPIService) {


    });